#include <iostream>
#include <chrono>
#include <thread>
#include "Graph.h"

int main(int argc, char *argv[])
{
    long sz = std::stoi(argv[1]);

    unsigned method = std::stoi(argv[2]);
    switch(method)
    {
        case 0:
        {
            Graph<DMatrix> G(sz);
            G.generate_random_graph(sz, 0.2);
            std::cout << G << std::endl;
            break;
        }
        case 1:
        {
            Graph<DMatrix> G(sz);
            G.generate_random_graph();
            std::cout << G << std::endl;
            break;
        }
        default:
        {
            Graph<DMatrix, true> G(sz);
            G.generate_random_dgraph();
            std::cout << G << std::endl;
            break;
        }
    }

    return 0;
}