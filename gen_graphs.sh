#!/bin/bash
EXEC_PATH="."
SCRIPT_PATH=".."
OUT_PATH="./data"
./$EXEC_PATH/random_graphs $1 0 | ./$SCRIPT_PATH/gen_graph_img.py $OUT_PATH/out0.png
./$EXEC_PATH/random_graphs $1 1 | ./$SCRIPT_PATH/gen_graph_img.py $OUT_PATH/out1.png
./$EXEC_PATH/random_graphs $1 2 | ./$SCRIPT_PATH/gen_graph_img.py $OUT_PATH/out2.png
