#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/Eigenvalues>
#include <unsupported/Eigen/MatrixFunctions>
#include <experimental/random>
#include <random>
#include <cmath>
#include <vector>
#include <algorithm>
/*...*/
#include <iostream>

using namespace Eigen;

using SMatrix = SparseMatrix<float>;
using DMatrix = Matrix<float, Dynamic, Dynamic>;

/** \class Graph
  *
  * \brief A simple Graph class.
  *
  * This class implements basic graph funcionality using both Sparse and Dense matrices as the storage
  * for the adjacency matrix.
  *
  * \warning SMatrix and DMatrix are just aliases for SparseMatrix and DenseMatrix with float32 as a scalar and
  * dynamic storage allocation.
  *
  * \tparam MatrixType Is either an SMatrix or a DMatrix.
  * \tparam isDirected Defines if the graph has directed or undirected edges.
  *
  */

template<typename MatrixType, bool isDirected=false>
class Graph
{
    /** \brief Adjacency matrix for the graph G.
     *
     * Given a particular graph if there is an edge from vertex u to vertex v then adjM_(u,v) = 1. In case
     * G is undirected, when \c isDirected is set to false, then (v,u) = 1 as well.
     */
    MatrixType adjM_;
    /** The number of nodes in the graph. */
    int nodes_;
    std::default_random_engine generator_;
    std::uniform_real_distribution<double> dist_;

    /** \details Used by \ref generate_random_dgraph to select randomly from a candidates or current node list
     * using \c delta as bias.
     *
     * \param candidates Possible set to select a vertex from.
     * \param node_list Possible set to select a vertex from.
     * \param delta Selection bias.
     *
     * \returns Returns the selected vertex.
     */
    long pick_node(std::vector<long>& candidates, std::vector<long>& node_list, double delta)
    {
        std::vector<long> out;

        if(delta > 0)
        {
            auto bias_sum = node_list.size()*delta;
            auto p_delta = bias_sum / (bias_sum + candidates.size());
            if(dist_(generator_) < p_delta)
            {
                std::sample(node_list.begin(), node_list.end(), std::back_inserter(out), 1, std::mt19937{std::random_device{}()});
                return out[0];
            }
        }
        std::sample(candidates.begin(), candidates.end(), std::back_inserter(out), 1, std::mt19937{std::random_device{}()});
        return out[0];
    }

    /** Helper function for \c operator<< */
    bool isDiGraph() const { return isDirected; }
public:
    /** Constructor that only starts the random number generators. */
    Graph()
    {
        generator_ = std::default_random_engine(123456);
        dist_ = std::uniform_real_distribution<double>(0,1);
    }

    /** \brief Constructor to initiate a Graph with a predefined number of nodes.
     *
     *  \arg nodes The number of nodes or vertices of the graph.
     */
    Graph(long nodes) : Graph()
    {
        alloc_adjM(nodes);
    }

    /** \brief Insert an edge on G.
     *
     *  If \c isDirected is set to true only (i,j) is added, otherwise (j,i) is also added.
     *  If i and j are equal no edge is inserted as the Graph is not expected to contain selfloops.
     *
     *  \arg i Vertex from where the edge goes in case of directed graphs.
     *  \arg j Vertex where the edge arrives in case of directed graphs.
     */
    void insert_edge(long i, long j)
    {
        if(i == j) return;
        if constexpr (std::is_same<MatrixType, SMatrix>::value)
        {
            /* coeffRef is the way to acces elements from a Sparse matrix. Notice that you can either insert elements
               via coeffRef or via insert however insert requires (i,j) to not being set. Since we are generating
               random graphs its hard to tell hence the use of coeffRef which checks that by itself.
            */
            adjM_.coeffRef(i,j) = 1;
            if constexpr (!isDirected) adjM_.coeffRef(j,i) = 1;
        } else {
            /* This is the way to access elements of a dense matrix on Eigen with range checking.
               Notice that if NDEBUG or EIGEN_NO_DEBUG is defined this approach and coeffRef are equivalent
            */
            adjM_(i,j) = 1;
            if constexpr (!isDirected) adjM_(j,i) = 1;
        }
    }

    /** \brief Initialize the adjacency matrix.
     *
     *  For dense matrices this is enough however sparse matrices won't have space available for exactly n^2 elements.
     *  See the documentation on \ref generate_random_graph(long,double) for more info.
     */
    void alloc_adjM(long nodes)
    {
        nodes_ = nodes;
        if constexpr (std::is_same<MatrixType, SMatrix>::value)
        {
            adjM_ = SMatrix(nodes_, nodes_);
        } else {
            adjM_ = DMatrix::Zero(nodes_, nodes_);
        }
    }

    /** \brief Returns the degree of a particular vertex.
     *
     *  Notice that for directed graphs this is equivalent to \ref in_degree.
     *
     *  \param node The vertex to get the degree.
     *  \returns The degree of vertex \c node.
     */
    long degree(long node)
    {
        long dg = 0;
        if constexpr (std::is_same<MatrixType, SMatrix>::value)
        {
            /* Here we use an iterator of the inner dimension to count vertex degree
               notice that the default storage order is ColumnMajor so the iterator goes
               through all incoming vertex to *node*.
            */
            for(SMatrix::InnerIterator it(adjM_, node); it; ++it) dg++;
        } else {
            /* Since the default storage order is ColumnMajor we grab a colum to count incoming vertices.
               For dense matrices we can get a column as a vector and perform one of the reduction operations,
               described on https://eigen.tuxfamily.org/dox/group__QuickRefPage.html#matrixonly, to
               quickly evaluate the degree.
            */
            return adjM_.col(node).sum();
        }
        return dg;
    }

    /** \brief The number of vertices that point towards \c node.
     *
     *  \param node The vertex to get the in degree.
     *  \returns The in degree of \c node.
     */
    long in_degree(long node) { return 2*degree(node); }

    /** \brief The number of vertices that \c node points toward.
     *
     *  \param node The vertex to get the out degree.
     *  \returns The out degree of \c node.
     */
    long out_degree(long node)
    {
        long dg = 0;
        if constexpr (std::is_same<MatrixType, SMatrix>::value)
        {
            /* Here we use an iterator of the inner dimension to count vertex degree
               notice that the default storage order is ColumnMajor so we need to transpose
               before grabbing the iterator. This might not be the most efficient code
               and it's here just to hammer down storage order.
            */
            SMatrix tAdjM = adjM_.transpose();
            for(SMatrix::InnerIterator it(tAdjM, node); it; ++it) dg++;
        } else {
            /* Since the default storage order is ColumnMajor we grab a row to count outgoing vertices.
               For dense matrices we can get a row as a vector and perform one of the reduction operations,
               described on https://eigen.tuxfamily.org/dox/group__QuickRefPage.html#matrixonly, to
               quickly evaluate the degree.
            */
            return adjM_.row(node).sum();
        }
        return dg;
    }

    /** \brief Generate random scale-free directed graph.
     *
     *  Based upon https://networkx.org/documentation/stable/_modules/networkx/generators/directed.html#scale_free_graph
     *
     *  Since \f$\alpha + \beta + \gamma = 1\f$ we don't let the user define \f$\gamma\f$, which represents the same as
     *  \f$\alpha\f$ but swapping in-degree distribution with the out-degree distribution.
     *
     *  \warning This function will generate an empty graph in case \c isDirected is false.
     *
     *  \param alpha Probability of adding a new vertex connected to an existing vertex following the in-degree distribuition.
     *  \param beta Probability of adding an edge between two existing nodes following the in/out-degree distribution.
     *  \param delta_in Bias for choosing in-degree distribution.
     *  \param delta_out Bias for selecting out-degree distribution.
     */
    void generate_random_dgraph(double alpha=0.41, double beta=0.54, double delta_in=0.2, double delta_out=0)
    {
        if constexpr (!isDirected) return;
        long m0 = nodes_*0.05 >= 3 ? nodes_*0.05 : 3;

        // A very hacky way of generating a K_m0
        generate_random_graph<false>(m0, 1);

        std::vector<long> in_dist, out_dist, nodes_list;
        for(auto i = 0; i < m0; i++)
        {
            auto idg = in_degree(i);
            auto odg = out_degree(i);
            if(idg)
            {
                for(auto j = 0; j < idg; j++)
                    in_dist.push_back(i);
            }
            if(odg)
            {
                for(auto j = 0; j < odg; j++)
                    out_dist.push_back(i);
            }
            nodes_list.push_back(i);
        }

        long cur = m0-1;
        while(nodes_list.size() <= nodes_)
        {
            double r = dist_(generator_);
            long v, w;
            if(r < alpha)
            {
                v = cur;
                nodes_list.push_back(cur);
                cur += 1;
                w = pick_node(in_dist, nodes_list, delta_in);
            } else if (r < alpha + beta)
            {
                v = pick_node(out_dist, nodes_list, delta_out);
                w = pick_node(in_dist, nodes_list, delta_in);
            } else {
                v = pick_node(out_dist, nodes_list, delta_out);
                w = cur;
                nodes_list.push_back(cur);
                cur += 1;
            }
            insert_edge(v,w);
            out_dist.push_back(v);
            in_dist.push_back(w);
        }
    }

    /** \brief Barabasi-Albert implementation.
     *
     *  \warning This function will generate an empty graph in case \c isDirected is true.
     */
    void generate_random_graph()
    {
        if constexpr (isDirected) return;

        long m0 = nodes_*0.01 >= 2 ? nodes_*0.01 : 2;
        long sum = 0;

        // Generating K_{m0}, this could be almost anything for Barabasi-Albert.
        adjM_.topLeftCorner(m0, m0) = MatrixXf::Constant(m0, m0, 1);
        adjM_.topLeftCorner(m0, m0) -= MatrixXf::Identity(m0, m0);

        sum = sum_degree();

        for(auto i = m0; i < nodes_; i++)
        {
            for(auto j = 0; j < i; j++)
            {
                auto k = degree(j);
                auto prob = k/static_cast<double>(sum);

                if(dist_(generator_) <= prob)
                {
                    insert_edge(i,j);
                    sum += 2;
                }
            }
        }
    }

    /** \brief Erdos-Renyi model
     *
     *  \tparam shouldReserve If MatrixType is a sparse matrix we need to reserve the expected number of nodes.
     *  \param nodes The number of vertices of the graph to be generated. This is not nodes_ because this function is used by others.
     *  \param p The probability of adding an edge.
     */
    template<bool shouldReserve=true>
    void generate_random_graph(long nodes, double p)
    {
        if constexpr (std::is_same<MatrixType, SMatrix>::value)
        {
            /* When populating sparse matrices each time you add a random element it takes roughly
               O(nnz), where nnz is the number of non-zero elements. The faster approach is to reserve
               the expected ammount of elements per column. For Erdos-Renyi the expected number
               of edges connected to a vertex is nodes*p.
            */
            if constexpr (shouldReserve) adjM_.reserve(VectorXi::Constant(nodes_, nodes*p));
        }

        for(auto i = 0; i < nodes; i++)
        {
            /* Although we can use this to generate a directed graph with some complicated properties
               it's recommended to stay with generate_random_dgraph for that. This is wy we start with j=i
               as the adjacency matrix of undirected graphs are symmetrical.
            */
            for(auto j = i; j < nodes; j++)
            {
                if(dist_(generator_) <= p)
                {
                    insert_edge(i,j);
                }
            }
        }
    }

    /** \brief Calculates the total degree of the graph.
     *
     *  This number is twice the number of edges. This is here just for the sake of
     *  demonstration as it could easily and more efficiently be calculated every time
     *  insert_edge is called.
     *
     *  \returns The total degree of the graph.
     */
    long sum_degree()
    {
        long sumDeg = 0;
        if constexpr (std::is_same<MatrixType, SMatrix>::value)
        {
            /* A normal for-loop with outerSize() will let you iterate over all cols (if storage order is ColumnMajor)
               or rows (if storage order if RowMajor)
            */
            for(auto i = 0; i < adjM_.outerSize(); i++)
            {
                sumDeg += degree(i);
            }
        } else {
            sumDeg = adjM_.sum();
        }
        return sumDeg;
    }

    friend std::ostream& operator<<(std::ostream& os, const Graph& g)
    {
        os << "@ " << g.isDiGraph() << std::endl;
        if constexpr (std::is_same<MatrixType, SMatrix>::value)
        {
            SMatrix T = g.adjM_.transpose();
            for(auto i = 0; i < T.outerSize(); i++)
            {
                os << i << " - ";
                for(SMatrix::InnerIterator it(T, i); it; ++it)
                {
                    os << it.row() << " ";
                }
                os << std::endl;
            }
        } else {
            for(auto i = 0; i < g.nodes_; i++)
            {
                os << i << " - ";
                for(auto j = 0; j < g.nodes_; j++)
                {
                    if(g.adjM_(i,j))
                        os << j << " ";
                }
                os << std::endl;
            }

        }
        return os;
    }

    // Sometimes the random graph has nodes without out edges making D non-invertible. Fix this.
    /**
     * \brief Force the adjacency matrix invertibility.
     *
     *  Sometimes generated random graphs have isolated vertices or, specially with directed graphs,
     *  some vertices have no incoming edges. When this happens adjM_ is no longer invertible which
     *  might make the centrality measures fail, specially Pagerank.
     *
     *  This functions go through all columns (vertices) and selects a random vertex to create an edge from it
     *  in case there are none. It also avoids generating selfloops.
     */
    void make_invertible() // Force strongly connectedness
    {
        if constexpr (std::is_same<MatrixType, SMatrix>::value)
        {
            for(auto i = 0; i < adjM_.outerSize(); i++)
            {
                SMatrix::InnerIterator it(adjM_,i);
                if(!it) // If this iterator is empty it means we have no non-zero elements in this column.
                {
                    auto nd = std::experimental::randint(0, nodes_-1);
                    // Don't generate selfloops
                    while(nd == i) nd = std::experimental::randint(0, nodes_-1);
                    adjM_.coeffRef(nd,i) = 1;
                }

            }
        } else {
            for(auto row = 0; row < adjM_.rows(); row++)
            {
                if(adjM_.row(row).sum() == 0)
                {
                    auto nd = std::experimental::randint(0, nodes_-1);
                    // Don't generate selfloops
                    while(nd == row) nd = std::experimental::randint(0, nodes_-1);
                    adjM_.row(row)[nd] = 1;
                }
            }
        }
    }

    // Slow but here for the sake of showing matrix power
    /** \brief Evaluates if there is a path of a fixed length between two nodes.
     *
     *  The approach here is to calculate \f$adjM\_^{pathLength}\f$.
     *
     *  \param u Starting vertex.
     *  \param v Ending vertex.
     *  \param pathLength The length of the path you want to check between u and v.
     *  \return true In case there is a path between \c u and \c v of length \c pathLength.
     *  \return false In case there are no paths between \c u and \c v of length \c pathLength.
     */
    bool pathFromTo(long u, long v, long pathLength)
    {
        if constexpr (std::is_same<MatrixType, SMatrix>::value) // diagnoal number of steps to come and go from u / outside number of paths from u to v
        {
            MatrixType P(adjM_);
            for(auto i = 1; i < pathLength; i++)
                P = P*P;
            return P.coeff(u,v) > 0;
        } else {
            MatrixType P = adjM_.pow(pathLength);
            return P.coeff(u,v) > 0;
        }
        return false;
    }

    double pagerank(VectorXf& v, long iters=15, double epsilon=1e-6, double d = 0.85)
    {
        v = VectorXf::Random(nodes_).normalized();

        make_invertible();

        MatrixType adjM_h = adjM_.transpose();
        MatrixType D;
        if constexpr (std::is_same<MatrixType, SMatrix>::value)
        {
            MatrixType ones(adjM_h);
            D = MatrixType(nodes_, nodes_);
            D.reserve(VectorXi::Constant(nodes_, 1));
            for(auto i = 0; i < nodes_; i++)
            {
                D.coeffRef(i,i) = 1.0/degree(i);
            }
            adjM_h = d*adjM_h*D + ones*(1-d)/nodes_;
        } else {
            D = adjM_h.colwise().sum().asDiagonal();

            adjM_h = d*adjM_h*D.inverse();
            adjM_h.array() += (1-d)/nodes_;
        }

        VectorXf v0 = v;

        v = adjM_h*v0;
        for(auto i = 0; i < iters; i++)
        {
            v0 = v;
            v = adjM_h*v0;
        }

        return (v - v0).norm();
    }

    /** \brief Eigenvector centrality measure
     *
     *
     *
     *  \tparam ComplexScalar The scalar type of the eigen decompostion.
     *  \tparam EigenVectorType The type of the eigenvector that is returned from the eigen decomposition.
     *
     *  \param v The eigenvector centrality measure for each vertex.
     *  \returns The corresponding eigenvalue of the eigenvector centrality measure.
     */
    template<typename ComplexScalar, typename EigenVectorType>
    ComplexScalar eigenvector_centrality(EigenVectorType& v)
    {
        if constexpr (isDirected || std::is_same<MatrixType, SMatrix>::value) return ComplexScalar(0);

        // adjM_ is symmetric so eigenvalues are all real
        EigenSolver<MatrixType> solver(adjM_);

        typename MatrixType::Index mIdx;
        solver.eigenvalues().real().maxCoeff(&mIdx);

        v = solver.eigenvectors().col(mIdx);

        return solver.eigenvalues()[mIdx];
    }
};
