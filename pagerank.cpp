#include <iostream>
#include <chrono>
#include <thread>

#include "Graph.h"

int main(int argc, char *argv[])
{
    long sz = std::stoi(argv[1]);
    VectorXf v2;

    Graph<SMatrix, true> G(sz);
    G.generate_random_dgraph();

    G.pagerank(v2);

    std::cout << G << std::endl;
    std::cout << "#" << std::endl;
    std::cout << v2 << std::endl;

    return 0;
}