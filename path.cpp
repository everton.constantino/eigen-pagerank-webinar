#include <iostream>
#include <chrono>
#include <thread>
#include "Graph.h"

int main(int argc, char *argv[])
{
    long sz = std::stoi(argv[1]);

    Graph<DMatrix> G(sz);
    unsigned method = std::stoi(argv[2]);
    switch(method)
    {
        case 0:
        {
            G.generate_random_graph(sz, 0.2);
            G.make_invertible();
            break;
        }
        default:
        {
            G.generate_random_graph();
            break;
        }
    }

    std::cout << G << std::endl;
    std::cout << "#" << std::endl;
    std::cout << G.pathFromTo(std::stoi(argv[3]), std::stoi(argv[4]), std::stoi(argv[5])) << std::endl;

    return 0;
}