#include <iostream>
#include <chrono>
#include <thread>
#include "Graph.h"

int main(int argc, char *argv[])
{
    long sz = std::stoi(argv[1]);

    Graph<DMatrix> G(sz);
    unsigned method = std::stoi(argv[2]);
    switch(method)
    {
        case 0:
        {
            G.generate_random_graph(sz, 0.2);
            G.make_invertible();
            break;
        }
        default:
        {
            G.generate_random_graph();
            G.make_invertible();
            break;
        }
    }

    std::cout << G << std::endl;
    std::cout << "#" << std::endl;
    typename EigenSolver<DMatrix>::EigenvectorsType v;
    std::cout << "\033[1;31m " \
        << "Eigenvalue: \033[0m " << G.eigenvector_centrality<EigenSolver<DMatrix>::ComplexScalar, EigenSolver<DMatrix>::EigenvectorsType>(v) \
        << std::endl;
    std::cout << "Eigenvector: " << std::endl << v << std::endl;
    std::cout << std::endl;
    return 0;
}