# Eigen Pagerank Webinar

A set of basic graph functions to show some Eigen features.

Run cmake, in case you have Eigen on a different directory set CMAKE_PREFIX_PATH.

Look inside the source code of each example to see how command line arguments are used.

There is a Python script available to read the output from the examples and generate images if the graphs used for the calculations.

