#!/bin/python3
import matplotlib.pyplot as plt
import networkx as nx
import sys

#lines = open(sys.argv[1]).readlines()
G = nx.DiGraph()
#for line in lines:
readingGraph = True
isDigraph = False
out=""
for line in sys.stdin:
    if line.strip():
        ls = line.strip().split(" ")
        if ls[0] == "#":
            readingGraph = False
            continue
        elif ls[0] == "@":
            isDigraph = int(ls[1]) > 0
            continue

        if readingGraph:
            u = int(ls[0])
            nds = ls[2:]
            for nd in nds:
                v = int(nd)
                G.add_edge(u,v)
        else:
            out += line

print(out)

nx.draw(G, arrows=isDigraph, with_labels=True, pos=nx.circular_layout(G))
plt.savefig(sys.argv[1])