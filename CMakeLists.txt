cmake_minimum_required(VERSION 3.0)
project(eigen_webinar)

find_package(Eigen3 3.3.90 REQUIRED NO_MODULE)

set(CMAKE_CXX_STANDARD 17)

add_executable(eigenvector_centrality eigenvector_centrality.cpp)
add_executable(pagerank pagerank.cpp)
add_executable(path path.cpp)
add_executable(random_graphs random_graphs.cpp)

target_link_libraries(eigenvector_centrality Eigen3::Eigen)
target_link_libraries(pagerank Eigen3::Eigen)
target_link_libraries(path Eigen3::Eigen)
target_link_libraries(random_graphs Eigen3::Eigen)